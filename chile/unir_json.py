import json, codecs

data0 = json.load(open('productos0.json'))["dataLayerGTM"]["productList"]
data30 = json.load(open('productos30.json'))["dataLayerGTM"]["productList"]
data60 = json.load(open('productos60.json'))["dataLayerGTM"]["productList"]
data90 = json.load(open('productos90.json'))["dataLayerGTM"]["productList"]
data120 = json.load(open('productos120.json'))["dataLayerGTM"]["productList"]
data150 = json.load(open('productos150.json'))["dataLayerGTM"]["productList"]
data180 = json.load(open('productos180.json'))["dataLayerGTM"]["productList"]
data210 = json.load(open('productos210.json'))["dataLayerGTM"]["productList"]
data240 = json.load(open('productos240.json'))["dataLayerGTM"]["productList"]
data270 = json.load(open('productos270.json'))["dataLayerGTM"]["productList"]
data300 = json.load(open('productos300.json'))["dataLayerGTM"]["productList"]
data330 = json.load(open('productos330.json'))["dataLayerGTM"]["productList"]
data360 = json.load(open('productos360.json'))["dataLayerGTM"]["productList"]
data390 = json.load(open('productos390.json'))["dataLayerGTM"]["productList"]
data420 = json.load(open('productos420.json'))["dataLayerGTM"]["productList"]
data450 = json.load(open('productos450.json'))["dataLayerGTM"]["productList"]
data480 = json.load(open('productos480.json'))["dataLayerGTM"]["productList"]

union_data = {"productos": data0 + data30 + data60 + data90 + data120 + data150 + data180 + data210 + data240 + data270 + data300 + data330 + data360 + data390 + data420 + data450 + data480 }

with open('output/productos.json', 'wb') as f:
    json.dump(union_data, codecs.getwriter('utf-8')(f), ensure_ascii=False)