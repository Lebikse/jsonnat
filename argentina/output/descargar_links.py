import requests
import json

links = json.load(open('productos_links.json'))

for x in links:
	with open(x.get('Nombre')+'.jpg', 'wb') as handle:
	        response = requests.get(x.get('Url'), stream=True)
	        print "Descargando: " + x.get('Nombre')+'.jpg'

	        if not response.ok:
	            print response

	        for block in response.iter_content(1024):
	            if not block:
	                break

	            handle.write(block)