import json, codecs
import re

data = json.load(open('productos.json'))['productos']


# for x in data:
# 	print x.get('name'), x.get('id'), x.get('longDescription')

# http://naturacosmeticos.com.ar/image/ar/sku/principal_641x449/26441_1.jpg

def build_structure(data, d=[]):
	for x in data:
		d.append({'Nombre': x.get('name'),'Id': x.get('id'), 'Url':'http://images.naturacosmeticos.com.ar/image/ar/sku/principal_641x449/'+re.sub('[^0-9]', '', x.get('id'))+'_1.jpg','Descripcion': x.get('description'),'DescripcionLarga': x.get('longDescription')})
	print d
	with open('productos_links.json', 'wb') as f:
		json.dump(d, codecs.getwriter('utf-8')(f), ensure_ascii=False)

build_structure(data)



