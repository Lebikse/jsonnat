import json, codecs


data0 = json.load(open('productos0.json'))["mainContent"]['productList']['products']
data30 = json.load(open('productos30.json'))["mainContent"]['productList']['products']
data60 = json.load(open('productos60.json'))["mainContent"]['productList']['products']
data90 = json.load(open('productos90.json'))["mainContent"]['productList']['products']
data120 = json.load(open('productos120.json'))["mainContent"]['productList']['products']
data150 = json.load(open('productos150.json'))["mainContent"]['productList']['products']
data180 = json.load(open('productos180.json'))["mainContent"]['productList']['products']
data210 = json.load(open('productos210.json'))["mainContent"]['productList']['products']
data240 = json.load(open('productos240.json'))["mainContent"]['productList']['products']
data270 = json.load(open('productos270.json'))["mainContent"]['productList']['products']
data300 = json.load(open('productos300.json'))["mainContent"]['productList']['products']
data330 = json.load(open('productos330.json'))["mainContent"]['productList']['products']
data360 = json.load(open('productos360.json'))["mainContent"]['productList']['products']
data390 = json.load(open('productos390.json'))["mainContent"]['productList']['products']
data420 = json.load(open('productos420.json'))["mainContent"]['productList']['products']
data450 = json.load(open('productos450.json'))["mainContent"]['productList']['products']
data480 = json.load(open('productos480.json'))["mainContent"]['productList']['products']

union_data = {"productos": data0 + data30 + data60 + data90 + data120 + data150 + data180 + data210 + data240 + data270 + data300 + data330 + data360 + data390 + data420 + data450 + data480 }

with open('output/productos.json', 'wb') as f:
    json.dump(union_data, codecs.getwriter('utf-8')(f), ensure_ascii=False)